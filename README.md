
Nubark Odoo Addons
==================

Public addons built by Nubark for multiple purposes.

[//]: # (addons)
addon | version | summary
--- | --- | ---
[addon_name](addon_name/) | 9.0.0.1.0 | Addon Summary

[//]: # (end addons)
